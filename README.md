# zloLauncherCLI #

## Description ##
A small (~50KiB without icon) command line interface (CLI) launcher for the zloemu project. Uses the most recent version of the zloAPI for BF3 and BF4.

## Features ##
* Load API (BF3 & BF4)
* Login (BF3 & BF4)
* List all servers (BF3 & BF4)
* Select server (BF3 & BF4)
* Basic server info (BF3 & BF4)
* Singleplayer (BF3 & BF4)
* Ping server (BF3 & BF4)
* Basic player stats (BF3 & BF4)
* Connect to server (BF3 & BF4)
* ServerLauncher with automatic refresh and reconnect and ErrorLogger (BF3)
* DLL-Updater/Downloader with automatic restart (BF3 & BF4)
____
## Usage ##
There are 15 commands of which you can pass 5 via command line (see below). Enter `help` to get a list.

`select`, `ping`, `info` and `connect` can either be used with the server ID you want to target or without an argument in which case the previously selected ID will be used (if any). 

1. Load a dll: `bf3` / `bf4`
2. Login with mail-address and password from zloemu
3. List servers: `servers` or play singleplayer: `single`
4. (optional) Select a server: `select X`
5. (optional) Print info: `info` / `info X`
6. (optional) Ping the server: `ping` / `ping X`
7. Connect to the server: `connect` / `connect X`

![01.png](https://bitbucket.org/repo/66MzqR/images/3765676048-01.png)

## Calling launcher from command line ##
You can pass 6 commands: `-bf3` or `-bf4` + `-login mailaddress password` + `-connect serverID` or `-single` + `-autoclose`

Login, connect/single and autoclose are optional. This will not work if your password starts with `-`, contains whitespaces or regular expressions like `\r` or `\n`

![02.png](https://bitbucket.org/repo/66MzqR/images/327534456-02.png)

## ServerLauncher ##
Either do it manually by loading a DLL with `bf3` / `bf4` and starting ServerMode with `server`, or call it from commandline/batch.
For example: copy DLL and launcher into your server-directory and insert following line at the beginning of your _StartServer.bat:
```
#!batch
start zloLauncherCLI.exe -bf3 -server

```
The status of your server(s) will auto-update. If you get disconnected from master-server launcher will automatically try to reconnect your server after 15seconds. Be aware that even if the server connects back immediately, it will still take some time (1-3 minutes) till you get back IN_GAME.
Exit ServerMode by pressing [CTRL] + [E] on your keyboard.

![03.png](https://bitbucket.org/repo/66MzqR/images/2900010704-03.png)
____

## Future ##
* ServerLauncher for BF4
* autorestart server if server crashes/hangs
* Cleanup