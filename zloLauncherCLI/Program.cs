﻿/*
    v1.2.3.0
    //
    BF3 - API 7.0.4
    BF4 - API 2.0.2
*/

using System;
using System.IO;
using System.Net;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;


namespace zloLauncherCLI
{
    public class Program
    {
        private static zloAPI.BF3 BF3dll;                                                                       // zloAPI BF3 predefined
        private static zloAPI.BF4 BF4dll;                                                                       // zloAPI BF4 predefined
        public static ManualResetEvent _waiter = new ManualResetEvent(true);                                    // wait-handle to block CLI until event from DLL is fired
        public static System.Timers.Timer _redrawtimer;                                                         // timer to refresh ServerLauncher a few seconds after server-attributes has changed
        public static System.Timers.Timer _reconnecttimer;                                                      // timer to automatically reconnect if ServerLauncher loses connection to master
        public static System.Timers.Timer _closetimer;                                                          // timer to automatically close launcher if game exits/crashes or restart server if server crashes
        private static int iLoaded = 0;                                                                         // toggle after dll loaded
        public static bool bLoggedin = false;                                                                   // toggle after logged in
        public static bool bServer = false;                                                                     // toggle after server mode initialised
        public static bool bSelectvalid = false;                                                                // toggle after valid server selection
        public static bool bClosetimer= false;                                                                  // toggle true if autoclose is enabled or autorestart gets disabled
        public static bool bExit = false;                                                                       // togle on shutdown-process
        public static int iLastServer = -1;                                                                     // last selected server 
        private static readonly Dictionary<string, string> RealMapNames = new Dictionary<string, string>();     // dictionary which will be searchable for the real map names
        private static readonly Dictionary<string, string> RealModeNames = new Dictionary<string, string>();    // dictionary which will be searchable for the real mode names
        public const int iZloLauncherID = 1337;                                                                 // registered static Launcher ID from zloemu.org
        public static int iUserID = 0;                                                                          // User ID
        public static string sUsername = "";                                                                    // User Name
        public static string sClantag = "";                                                                     // Clantag
        public static int iDogtag1 = 0;                                                                         // basic Dogtag
        public static int iDogtag2 = 0;                                                                         // advanced Dogtag
        public static int iLastEvent = -1;                                                                      // stores the latest zevent from zloAPI.BFX.EventListener()
        private static string sParameters = "";                                                                 // stores all arguments which were passed via commandline so launcher can be recalled by UpdateDLL
        public static bool bFailedCheck = false;


        // entry point
        static void Main(string[] args)
        {
            // enable UTF-8 support
            Console.OutputEncoding = Encoding.UTF8;
            // set size and title
            Console.WindowWidth = 120;
            Console.WindowHeight = 30;
            Console.Title = "zloLauncherCLI v1.2.3";
            // ASCII-Art created with http://patorjk.com/software/taag/
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(@"           __      __                           __              ________    ____");
            Console.WriteLine(@"    ____  / /___  / /   ____ ___  ______  _____/ /_  ___  _____/ ____/ /   /  _/");
            Console.WriteLine(@"   /_  / / / __ \/ /   / __ `/ / / / __ \/ ___/ __ \/ _ \/ ___/ /   / /    / /  ");
            Console.WriteLine(@"    / /_/ / /_/ / /___/ /_/ / /_/ / / / / /__/ / / /  __/ /  / /___/ /____/ /   ");
            Console.WriteLine(@"   /___/_/\____/_____/\__,_/\__,_/_/ /_/\___/_/ /_/\___/_/   \____/_____/___/   ");
            Console.WriteLine(@"                                                                                ");
            Console.WriteLine("                        [ a CLI Launcher for zloemu.org ]");
            Console.WriteLine("                                   - by Despo -");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Gray;

            // set up eventhandler for ProcessExit
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);

            if (args.Length > 0)
                CLIargs(args);

            Launcher();
        }


        // on exit of program
        static void OnProcessExit(object sender, EventArgs e)
        {
            bExit = true;
            BF3dll = null;
            BF4dll = null;
        }


        // main program
        private static void Launcher()
        {
            while (true)
            {
                CLI();
                Console.WriteLine();
            }
        }


        // main interface
        private static void CLI()
        {
            string cmd = null;

            // if no arguments passed then prompt input
            while (string.IsNullOrEmpty(cmd))
            {
                PrintPromt();
                cmd = Console.ReadLine();
            }

            cmd = cmd.ToLower();
            string[] args = cmd.Split(' ');

            switch (args[0])
            { 
                case "help":
                    Console.WriteLine("zloLauncherCLI v1.2.3 by Despo");
                    Console.WriteLine();
                    Console.WriteLine("AUTOCLOSE    - automatically close launcher if game closes/crashes");
                    //Console.WriteLine("AUTORES      - automatically restart server if server crashes");
                    Console.WriteLine("BF3          - loads DLL for Battlefield 3");
                    Console.WriteLine("BF4          - loads DLL for Battlefield 4");
                    Console.WriteLine("CLEAR        - clears this window");
                    Console.WriteLine("CONNECT      - connects to a selected serverID");
                    Console.WriteLine("EXIT         - exits this program");
                    Console.WriteLine("HELP         - shows this helpfile");
                    Console.WriteLine("INFO         - shows basic information about a selected server");
                    Console.WriteLine("LOGIN        - start ClientLauncher");
                    Console.WriteLine("PING         - pings a selected server");
                    Console.WriteLine("SELECT       - select a server by ID");
                    Console.WriteLine("SERVER       - start ServerLauncher");
                    Console.WriteLine("SERVERS      - lists all servers");
                    Console.WriteLine("SINGLE       - starts singleplayer of the game");
                    Console.WriteLine("STATS        - shows basic player statistics");
                    break;

                case "autoclose":
                    bClosetimer = !bClosetimer;
                    PrintInfo("AutoClose: " + bClosetimer);
                    break;

                case "bf3":
                    LoadBF3dll();
                    break;

                case "bf4":
                    LoadBF4dll();
                    break;

                case "clear":
                    Console.Clear();
                    break;

                case "connect":
                    if (args.Length > 1)
                        Select(args[1]);
                    Connect();
                    break;

                case "exit":
                    try
                    {
                        bExit = true;
                        BF3dll = null;
                        BF4dll = null;
                        Environment.Exit(0);
                    }
                    catch (Exception)
                    {
                        Environment.Exit(0);
                    }
                    break;

                case "info":
                    if (args.Length > 1)
                        Info(args[1]);
                    else
                        Info();
                    break;

                case "login":
                    Login();
                    break;

                case "ping":
                    if (args.Length > 1)
                        Ping(args[1]);
                    else
                        Ping();
                    break;

                case "select":
                    if (args.Length > 1)
                        Select(args[1]);
                    else
                        Select();
                    break;

                case "server":
                    Server();
                    break;

                case "servers":
                    Servers();
                    break;

                case "single":
                    Single();
                    break;

                case "stats":
                    Stats();
                    break;

                default:
                    PrintError("'" + args[0] + "' is not a valid command");
                    break;
            }
        }


        // commandline launch
        private static void CLIargs(string[] args)
        {
            // save all arguments
            for (int j = 0; j < args.Length; j++)
            {
                sParameters += args[j] + " ";
            }

            // we have to set it here because otherwise it will be set after Connect()/Single() has been called
            if (args.Contains("-autoclose"))
                bClosetimer = true;

            // process all passed arguments
            for (int i = 0; i < args.Length; i++)
            {
                // skip non-command args
                if (!args[i].StartsWith("-"))
                    continue;

                // doesn't matter if commands are passed in lower- or uppercase
                args[i] = args[i].ToLower();

                switch (args[i])
                {
                    // 'zloLauncherCLI.exe -bf3 -login usermail password -connect sID -autoclose'
                    case "-autoclose":
                        bClosetimer = true;
                        break;

                    case "-bf3":
                        PrintInfo("Loading BF3 dll...");
                        LoadBF3dll();
                        break;

                    case "-bf4":
                        PrintInfo("Loading BF4 dll...");
                        LoadBF4dll();
                        break;

                    case "-connect":
                        // 'zloLauncherCLI.exe -bf3 -login usermail password -connect sID'
                        if (args.Length > 5)
                        {
                            if (iLoaded != 0)
                            {
                                Select(args[i + 1]);
                                if (bSelectvalid)
                                    Connect();
                            }
                        }
                        break;

                    case "-login":
                        // 'zloLauncherCLI.exe -bf3 -login usermail password'
                        if (args.Length > 3)
                        {
                            PrintInfo("Logging in...");
                            Login(args[i + 1], args[i + 2]);
                        }
                        else
                            PrintError("mail or password not provided!");
                        break;

                    case "-server":
                        // 'zloLauncherCLI.exe -bf3 -server'
                        Server();
                        break;

                    case "-single":
                        Single();
                        break;

                    default:
                        PrintError("'" + args[i] + "' is not a valid command");
                        break;
                }
            }

            // clear saved parameters if no update has been done
            sParameters = null;
        }


        // AUTOCLOSE
        // autoclose on game/server crash/exit
        private static bool CloseHandler(string name, string title)
        {
            // unfortunately we can't use a callback-function on Process.Exited because it will only trigger if process exits normally
            // if process crashes, we won't get any EventRaised, so we HAVE to poll the process every few seconds to see if it's still responding

            // get the process of game/server by executable-name
            var proc = Process.GetProcessesByName(name);
            if (proc.Length < 1)
                return false;

            // check if executable-title matches given title
            if (!proc[0].MainWindowTitle.StartsWith(title))
                return false;

            // check if process has valid handle
            if (proc[0].MainWindowHandle == IntPtr.Zero)
                return false;

            // set parameters for timer
            object par1 = bClosetimer;                                                  // true for autoclose-mode, false for autorestart-mode
            object par2 = name;
            object par3 = title;
            object parameters = new[] { par1, par2, par3};

            // setup and start timer
            _closetimer = new System.Timers.Timer();
            _closetimer.Interval = 6000;
            _closetimer.AutoReset = true;
            _closetimer.Elapsed += (source, e) => CloseTimer(source, e, parameters);
            _closetimer.Enabled = true;

            return true;
        }


        // BF3
        // initialize API for BF3
        private static void LoadBF3dll()
        {
            // check if DLL has been loaded already
            if (iLoaded != 0)
            {
                PrintError("DLL already loaded!");
                return;
            }

            // check for Launcher.dll
            if (!File.Exists("LauncherBF3.dll"))
            {
                PrintError("LauncherBF3.dll not found!");
                // try to download DLL
                bool b = UpdateDLL(1);

                // recall function
                if (b)
                    LoadBF3dll();
                // exit here after recalled function exits
                return ;
            }

            // open dll
            BF3dll = new zloAPI.BF3();
            iLoaded = 1;

            // fill Map- and Mode-Dictionary with values
            for (int i = 0; i < 29; i++)
            {
                RealMapNames.Add(Data.BF3data.MapNames[i], Data.BF3data.RealMapNames[i]);
            }
            for (int i = 0; i < 16; i++)
            {
                RealModeNames.Add(Data.BF3data.ModeNames[i], Data.BF3data.RealModeNames[i]);
            }
        }


        // BF4
        // initialize API for BF4
        private static void LoadBF4dll()
        {
            // check if DLL has been loaded already
            if (iLoaded != 0)
            {
                PrintError("DLL already loaded!");
                return;
            }

            // check for Launcher.dll
            if (!File.Exists("LauncherBF4.dll"))
            {
                PrintError("LauncherBF4.dll not found!");
                // try to download DLL
                bool b = UpdateDLL(2);

                // recall function
                if (b)
                    LoadBF4dll();
                // exit here after recalled function exits
                return;
            }

            // open dll
            BF4dll = new zloAPI.BF4();
            iLoaded = 2;

            // fill Map- and Mode-Dictionary with values
            for (int i = 0; i < 33; i++)
            {
                RealMapNames.Add(Data.BF4data.MapNames[i], Data.BF4data.RealMapNames[i]);
            }
            for (int i = 0; i < 18; i++)
            {
                RealModeNames.Add(Data.BF4data.ModeNames[i], Data.BF4data.RealModeNames[i]);
            }
        }


        // CONNECT
        private static void Connect()
        {
            // check if we are logged in
            if (!bLoggedin)
            {
                PrintError("User not logged in!");
                return;
            }

            // check if server was selected successfully
            if (!bSelectvalid)
            {
                PrintError("No server selected!");
                return;
            }

            // set waiter which gets resetted when ClientListener() or EventListener() triggers
            _waiter.Reset();

            int iState = -1;

            // run multiplayer
            if (iLoaded == 1)
                iState = zloAPI.BF3.ZLO_RunMulti();
            else if (iLoaded == 2)
                iState = zloAPI.BF4.ZLO_RunMulti(false);

            if (iState != 0)
                PrintError("Could not start the client! " + iState);
            else
            {
                // waits a maximum of 90sec for the game to load level
                if (!_waiter.WaitOne(90000))
                    PrintError("Timed out while trying to connect to server! Try again later.");
                // set autoclose if game loaded successfully
                else
                {
                    if (bClosetimer)
                    {
                        if (iLoaded == 1)
                        {
                            if (!CloseHandler("bf3", "Battlefield 3™"))
                                PrintError("Failed to monitor game!");
                        }
                        else if (iLoaded == 2)
                        {
                            if (!CloseHandler("bf4_x86", "Battlefield 4"))
                                PrintError("Failed to monitor game!");
                        }
                    }
                }
            }

            // set the timer back to non-blocking
            _waiter.Set();
        }


        // LOGIN
        private static void Login(string sMail = null, string sPassword = null)
        {
            // check if DLL has been loaded
            if (iLoaded == 0)
            {
                PrintError("No DLL loaded!");
                return;
            }

            // check if already logged in
            if (bLoggedin)
            {
                PrintError("Already logged in!");
                return;
            }

            if (sMail == null || sMail.Length < 2 || sPassword == null || sPassword.Length < 2)
            {
                PrintInput("Login: Enter mail:");
                sMail = Console.ReadLine();

                PrintInput("Login: Enter password:");
                sPassword = "";

                // do not show password chars
                ConsoleKeyInfo key = Console.ReadKey(true);
                while (key.Key != ConsoleKey.Enter)
                {
                    if (key.Key != ConsoleKey.Backspace)
                        sPassword += key.KeyChar;
                    else if (key.Key == ConsoleKey.Backspace)
                    {
                        if (!string.IsNullOrEmpty(sPassword))
                        {
                            // remove last char from pw
                            sPassword = sPassword.Substring(0, sPassword.Length - 1);
                        }
                    }
                    key = Console.ReadKey(true);
                }
                Console.WriteLine();
            }

            // BF3
            if (iLoaded == 1)
            {
                // connect to master
                if (!BF3dll.ConnectClient())
                {
                    PrintError("Could not connect to master!");
                    return;
                }

                // reset waiter so CLI will stop until waiter gets set
                _waiter.Reset();

                BF3dll.LoginClient(sMail, sPassword);
            }
            // BF4
            else if (iLoaded == 2)
            {
                // connect to master
                if (!BF4dll.ConnectClient())
                {
                    PrintError("Could not connect to master!");
                    return;
                }

                // reset waiter so CLI will stop until waiter gets set
                _waiter.Reset();

                BF4dll.LoginClient(sMail, sPassword);
            }

            // wait for the serverlist to finish loading
            // waits a maximum of 10sec for the EventListener to trigger an event which will set back the waiter
            if (!_waiter.WaitOne(10000))
            {
                PrintError("Timed out while trying to login! Try again later.");

                // set the timer back to non-blocking
                _waiter.Set();
                return;
            }

            // set username + clantag to title
            if (!string.IsNullOrEmpty(sClantag))
                Console.Title = "zloLauncherCLI v1.2.3 - [" + sClantag + "] " + sUsername;
            else
                Console.Title = "zloLauncherCLI v1.2.3 - " + sUsername;
        }


        // INFO
        // prints basic server informations
        private static void Info(string sID = null)
        {
            // check server ID
            int iID = ChecksID(sID);
            if (iID == -1)
                return;

            // BF3
            if (iLoaded == 1)
            {
                Console.WriteLine();
                Console.WriteLine("ID:          {0}", iID);
                Console.WriteLine("IP:          {0}", zloAPI.BF3.ServerList[iID].ip);
                Console.WriteLine("STATE:       {0}", zloAPI.BF3.ServerList[iID].state);
                Console.WriteLine("NAME:        {0}", zloAPI.BF3.ServerList[iID].name);
                Console.WriteLine("PRESET:      {0}", zloAPI.BF3.ServerList[iID].attr["preset"]);
                Console.WriteLine("MAP:         {0}", zloAPI.BF3.ServerList[iID].attr["level"]);
                Console.WriteLine("GAMEMODE:    {0}", zloAPI.BF3.ServerList[iID].attr["levellocation"]);
                Console.WriteLine("GAMESIZE:    {0}", zloAPI.BF3.ServerList[iID].gamesize);
                Console.WriteLine("PLAYERS:     {0}", zloAPI.BF3.ServerList[iID].playercount);
                Console.WriteLine("FAILBUSTER:  {0}", zloAPI.BF3.ServerList[iID].attr["punkbuster"]);
                Console.WriteLine("DESCRIPTION: {0}", zloAPI.BF3.ServerList[iID].attr["description1"]);
            }
            // BF4
            else if (iLoaded == 2)
            {
                Console.WriteLine();
                Console.WriteLine("ID:          {0}", iID);
                Console.WriteLine("IP:          {0}", zloAPI.BF4.ServerList[iID].ip);
                Console.WriteLine("STATE:       {0}", zloAPI.BF4.ServerList[iID].state);
                Console.WriteLine("NAME:        {0}", zloAPI.BF4.ServerList[iID].name);
                Console.WriteLine("TICK RATE:   {0}", zloAPI.BF4.ServerList[iID].attr["tickRate"] + " Hz");
                Console.WriteLine("TYPE:        {0}", zloAPI.BF4.ServerList[iID].attr["servertype"]);
                Console.WriteLine("PRESET:      {0}", zloAPI.BF4.ServerList[iID].attr["preset"]);
                Console.WriteLine("MAP:         {0}", zloAPI.BF4.ServerList[iID].attr["level"]);
                Console.WriteLine("GAMEMODE:    {0}", zloAPI.BF4.ServerList[iID].attr["levellocation"]);
                Console.WriteLine("GAMESIZE:    {0}", zloAPI.BF4.ServerList[iID].gamesize);
                Console.WriteLine("PLAYERS:     {0}", zloAPI.BF4.ServerList[iID].playercount);
                Console.WriteLine("FAILBUSTER:  {0}", zloAPI.BF4.ServerList[iID].attr["punkbuster"]);
                Console.WriteLine("FAIRFIGHT:   {0}", zloAPI.BF4.ServerList[iID].attr["fairfight"]);
                Console.WriteLine("DESCRIPTION: {0}", zloAPI.BF4.ServerList[iID].attr["description1"]);
            }

        }


        // PING
        private static void Ping(string sID = null)
        {
            // check server ID
            int iID = ChecksID(sID);
            if (iID == -1)
                return;

            string ip = "";

            // get IP
            if (iLoaded == 1)
                ip = zloAPI.BF3.ServerList[iID].ip;
            else if (iLoaded == 2)
                ip = zloAPI.BF4.ServerList[iID].ip;

            if (ip.Length > 6)
            {
                try
                {
                    Ping ping = new Ping();
                    PingReply pingReply = ping.Send(ip, 4000);
                    PrintInfo("Server ID " + iID + " - IP " + ip + " - " + pingReply.RoundtripTime + "ms");
                }
                catch (Exception)
                {
                    PrintError("Could not ping server!");
                }
            }
            else
                PrintError("IP not valid!");
        }


        // SELECT
        private static void Select(string sID = null)
        {
            // check server ID
            int iID = ChecksID(sID);
            if (iID == -1)
                return;

            // set lastserver to selected one
            iLastServer = iID;
            // set waiter which gets resetted when EventListener() triggers
            _waiter.Reset();

            // actually select the server
            if (iLoaded == 1)
                zloAPI.BF3.ZLO_SelectServer(iID);
            else if (iLoaded == 2)
                zloAPI.BF4.ZLO_SelectServer(iID);

            // waits a maximum of 10sec for zloAPI.BF3.EventListener to get triggered
            if (!_waiter.WaitOne(10000))
            {
                PrintError("Timed out while trying to select server! Try again later.");

                // set the timer back to non-blocking
                _waiter.Set();
            }
        }


        // SERVER
        // start launcher as serverlauncher
        private static void Server()
        {
            // check if DLL has been loaded
            if (iLoaded == 0)
            {
                PrintError("No DLL loaded!");
                return;
            }

            // BF3
            if (iLoaded == 1)
            {
                // start listening for running servers
                if (!bServer)
                {
                    if (!zloAPI.BF3.ZLO_ListenServer())
                    {
                        PrintLogError("Can't open port for server!");
                        return;
                    }
                }
                // set toggle so we won't call ZLO_ListenServer() again
                bServer = true;

                // connect to master
                if (!BF3dll.ConnectServer())
                {
                    PrintLogError("Can't connect to master!");
                    return;
                }
            }
            // BF4
            else if (iLoaded == 2)
            {
                // start listening for running servers
                if (!bServer)
                {
                    if (!zloAPI.BF4.ZLO_ListenServer())
                    {
                        PrintLogError("Can't open port for server!");
                        return;
                    }
                }
                // set toggle so we won't call ZLO_ListenServer() again
                bServer = true;

                // connect to master
                if (!BF4dll.ConnectServer())
                {
                    PrintLogError("Can't connect to master!");
                    return;
                }
            }

            // reset waiter so CLI will stop until waiter gets set
            _waiter.Reset();

            // set up timers
            _reconnecttimer = new System.Timers.Timer();
            _reconnecttimer.Enabled = false;
            _reconnecttimer.Interval = 5000;
            _reconnecttimer.Elapsed += new ElapsedEventHandler(ReconnectTimer);
            _redrawtimer = new System.Timers.Timer();
            _redrawtimer.Enabled = false;
            _redrawtimer.Interval = 2000;
            _redrawtimer.Elapsed += new ElapsedEventHandler(RedrawTimer);

            // wait for the launher to connect to master (zevent 34)
            // waits a maximum of 10sec for the EventListener to trigger an event which will set back the waiter
            if (!_waiter.WaitOne(10000))
            {
                PrintLogError("Timed out while trying to connect to master! Try again later.");

                // set the timer back to non-blocking
                _waiter.Set();
                return;
            }

            PrintInfo("ServerLauncher mode! Exit by pressing [Ctrl] + [E].");
            PrintInfo("Waiting for servers...");

            // prevent accidentally closing
            PrevenClosing();

            // stay inside of ServerLauncher-mode until user presses [CTRL]+[E]
            ConsoleKeyInfo key = Console.ReadKey(true);
            while (true)
            {
                if (key.Key == ConsoleKey.E && key.Modifiers == ConsoleModifiers.Control)
                    break;
                key = Console.ReadKey(true);
            }

            Console.WriteLine();
            PrintInfo("Servermode terminated!");
            _reconnecttimer.Enabled = false;
            _redrawtimer.Enabled = false;
            _reconnecttimer = null;
            _redrawtimer = null;
            bServer = false;
        }


        // SERVERS
        // list all servers from zloAPI.BF3.ServerList
        private static void Servers()
        {
            // check if we are logged in
            if (!bLoggedin)
            {
                PrintError("User not logged in!");
                return;
            }

            // BF3
            if (iLoaded == 1)
            {
                Console.WriteLine();
                Console.WriteLine(" ID     SERVERNAME                                                 MAP                  GAMETYPE                PLAYERS");
                Console.WriteLine(" -----  ---------------------------------------------------------  -------------------  ----------------------  -------");
                foreach (var server in zloAPI.BF3.ServerList)
                {
                    // cut servername to a maximum of 5 chars
                    string sName = server.Value.name;
                    if (sName.Length > 57)
                        sName = sName.Substring(0, 57);
                    string sPlayers = "";

                    // add leading '0' if < 10 players or slots
                    if (server.Value.playercount < 10)
                        sPlayers = "0";
                    sPlayers += server.Value.playercount + "/";
                    if (server.Value.gamesize < 10)
                        sPlayers += "0";
                    sPlayers += server.Value.gamesize;

                    // handle new or unknown maps/modes
                    string sRealMap = server.Value.attr["level"];
                    if (RealMapNames.ContainsKey(sRealMap))
                        sRealMap = RealMapNames[sRealMap];
                    string sRealMode = server.Value.attr["levellocation"];
                    if (RealModeNames.ContainsKey(sRealMode))
                        sRealMode = RealModeNames[sRealMode];

                    // IDs should be smaller than 6 chars, servername has been cutted to 57 chars, longest mapname has 19 chars (Operation Firestorm)
                    // longest gametype has 22 chars (Conquest Assault Large) and gamesize is always 5 chars
                    Console.WriteLine(" {0,-5}  {1,-57}  {2,-19}  {3,-22}  {4,-5}", server.Key, sName, sRealMap, sRealMode,sPlayers);
                }
            }
            // BF4
            else if (iLoaded == 2)
            {
                Console.WriteLine();
                Console.WriteLine(" ID     SERVERNAME                                                MAP                    GAMETYPE               PLAYERS");
                Console.WriteLine(" -----  --------------------------------------------------------  ---------------------  ---------------------  -------");
                foreach (var server in zloAPI.BF4.ServerList)
                {
                    // cut servername to a maximum of 56 chars
                    string sName = server.Value.name;
                    if (sName.Length > 56)
                        sName = sName.Substring(0, 56);
                    string sPlayers = "";

                    // add leading '0' if < 10 players or slots
                    if (server.Value.playercount < 10)
                        sPlayers = "0";
                    sPlayers += server.Value.playercount + "/";
                    if (server.Value.gamesize < 10)
                        sPlayers += "0";
                    sPlayers += server.Value.gamesize;

                    // handle new or unknown maps/modes
                    string sRealMap = server.Value.attr["level"];
                    if (RealMapNames.ContainsKey(sRealMap))
                        sRealMap = RealMapNames[sRealMap];
                    string sRealMode = server.Value.attr["levellocation"];
                    if (RealModeNames.ContainsKey(sRealMode))
                        sRealMode = RealModeNames[sRealMode];

                    // IDs should be smaller than 6 chars, servername has been cutted to 56 chars, longest mapname has 21 chars (Zavod:Graveyard Shift)
                    // longest gametype has 21 chars (Carrier Assault Large) and gamesize is always 5 chars
                    Console.WriteLine(" {0,-5}  {1,-56}  {2,-21}  {3,-21}  {4,-5}", server.Key, sName, sRealMap, sRealMode, sPlayers);
                }
            }
        }


        // SINGLE
        private static void Single()
        {
            // check if we are logged in
            if (!bLoggedin)
            {
                PrintError("User not logged in!");
                return;
            }

            int iState = -1;

            // run singleplayer
            if (iLoaded == 1)
                iState = zloAPI.BF3.ZLO_RunSingle();
            else if (iLoaded == 2)
                iState = zloAPI.BF4.ZLO_RunSingle();

            if (iState != 0)
                PrintError("Could not start the client! " + iState);
            else
            {
                // waits a maximum of 45sec for the game to load level
                if (!_waiter.WaitOne(45000))
                    PrintError("Timed out while trying to load game! Try again later.");
                // set autoclose if game loaded successfully
                else
                {
                    if (bClosetimer)
                    {
                        if (iLoaded == 1)
                        {
                            if (!CloseHandler("bf3", "Battlefield 3™"))
                                PrintError("Failed to monitor game!");
                        }
                        else if (iLoaded == 2)
                        {
                            if (!CloseHandler("bf4_x86", "Battlefield 4") || !CloseHandler("bf4", "Battlefield 4"))
                                PrintError("Failed to monitor game!");
                        }
                    }
                }
            }
        }


        // STATS
        // prints basic server informations
        private static void Stats()
        {
            // check if we are logged in
            if (!bLoggedin)
            {
                PrintError("User not logged in!");
                return;
            }

            // set waiter which gets resetted when EventListener() triggers
            _waiter.Reset();

            // BF3
            if (iLoaded == 1)
            {
                // gather all player stats
                zloAPI.BF3.ZLO_GetStats();
            }
            // BF4
            else if (iLoaded == 2)
            {
                // gather all player stats
                zloAPI.BF4.ZLO_GetStats();
            }

            // waits a maximum of 10sec for zloAPI.BFX.EventListener to get triggered
            if (!_waiter.WaitOne(10000))
            {
                PrintError("Timed out while waiting for player stats! Try again later.");

                // set the timer back to non-blocking
                _waiter.Set();
                return;
            }

            // check if we received stats-end message
            if (iLastEvent != 37)
            {
                PrintError("Stats corrupted! Try again later.");
                return;
            }

            // basic values
            int totalscore = 0;
            int awardscore = 0;
            int combatscore = 0;
            int supportsc = 0;
            int assaultsc = 0;
            int engineersc = 0;
            int reconsc = 0;
            int kills = 0;
            int deaths = 0;
            int skill = 0;
            int rank = 0;

            // BF3
            if (iLoaded == 1)
            {
                // classes scores
                if (zloAPI.BF3.PlayerStats.ContainsKey("sc_support"))
                {
                    supportsc = (int) zloAPI.BF3.PlayerStats["sc_support"];
                    combatscore += supportsc;
                }
                if (zloAPI.BF3.PlayerStats.ContainsKey("sc_assault"))
                {
                    assaultsc = (int) zloAPI.BF3.PlayerStats["sc_assault"];
                    combatscore += assaultsc;
                }
                if (zloAPI.BF3.PlayerStats.ContainsKey("sc_engineer"))
                {
                    engineersc = (int) zloAPI.BF3.PlayerStats["sc_engineer"];
                    combatscore += engineersc;
                }
                if (zloAPI.BF3.PlayerStats.ContainsKey("sc_recon"))
                {
                    reconsc = (int) zloAPI.BF3.PlayerStats["sc_recon"];
                    combatscore += reconsc;
                }

                // awardscore
                if (zloAPI.BF3.PlayerStats.ContainsKey("sc_award"))
                    awardscore = (int) zloAPI.BF3.PlayerStats["sc_award"];

                // additional scores
                if (zloAPI.BF3.PlayerStats.ContainsKey("sc_unlock"))
                    totalscore += (int) zloAPI.BF3.PlayerStats["sc_unlock"];

                // vehicle scores
                // .
                // .
                // .

                totalscore += combatscore;
                totalscore += awardscore;

                // numeric values
                if (zloAPI.BF3.PlayerStats.ContainsKey("c___k_g"))
                    kills = (int) zloAPI.BF3.PlayerStats["c___k_g"];
                if (zloAPI.BF3.PlayerStats.ContainsKey("c___d_g"))
                    deaths = (int) zloAPI.BF3.PlayerStats["c___d_g"];
                if (zloAPI.BF3.PlayerStats.ContainsKey("elo"))
                    skill = (int) zloAPI.BF3.PlayerStats["elo"];
                rank = (int) zloAPI.BF3.PlayerStats["rank"];
            }
            // BF4
            else if (iLoaded == 2)
            {
                // classes scores
                if (zloAPI.BF4.PlayerStats.ContainsKey("sc_support"))
                {
                    supportsc = (int)zloAPI.BF4.PlayerStats["sc_support"];
                    combatscore += supportsc;
                }
                if (zloAPI.BF4.PlayerStats.ContainsKey("sc_assault"))
                {
                    assaultsc = (int)zloAPI.BF4.PlayerStats["sc_assault"];
                    combatscore += assaultsc;
                }
                if (zloAPI.BF4.PlayerStats.ContainsKey("sc_engineer"))
                {
                    engineersc = (int)zloAPI.BF4.PlayerStats["sc_engineer"];
                    combatscore += engineersc;
                }
                if (zloAPI.BF4.PlayerStats.ContainsKey("sc_recon"))
                {
                    reconsc = (int)zloAPI.BF4.PlayerStats["sc_recon"];
                    combatscore += reconsc;
                }

                // awardscore
                if (zloAPI.BF4.PlayerStats.ContainsKey("sc_award"))
                    awardscore = (int)zloAPI.BF4.PlayerStats["sc_award"];

                // additional scores
                if (zloAPI.BF4.PlayerStats.ContainsKey("sc_unlock"))
                    totalscore += (int)zloAPI.BF4.PlayerStats["sc_unlock"];

                // vehicle scores
                // .
                // .
                // .

                totalscore += combatscore;
                totalscore += awardscore;

                // numeric values
                if (zloAPI.BF4.PlayerStats.ContainsKey("c___k_g"))
                    kills = (int)zloAPI.BF4.PlayerStats["c___k_g"];
                if (zloAPI.BF4.PlayerStats.ContainsKey("c___d_g"))
                    deaths = (int)zloAPI.BF4.PlayerStats["c___d_g"];
                if (zloAPI.BF4.PlayerStats.ContainsKey("skill"))
                    skill = (int)zloAPI.BF4.PlayerStats["skill"];
                rank = (int) zloAPI.BF4.PlayerStats["rank"];
            }

            Console.WriteLine();
            Console.WriteLine("NAME:         {0}", sUsername);
            Console.WriteLine("CLANTAG:      {0}", sClantag);
            Console.WriteLine("ID:           {0}", iUserID);
            Console.WriteLine("DOGTAGS:      {0}, {1}", iDogtag1, iDogtag2);
            // vehicle score is missing from total score
            Console.WriteLine("TOTAL SCORE:  {0}", totalscore);
            Console.WriteLine("COMBAT SCORE: {0}", combatscore);
            Console.WriteLine("AWARD SCORE:  {0}", awardscore);
            Console.WriteLine("KILLS:        {0}", kills);
            Console.WriteLine("DEATHS:       {0}", deaths);
            Console.WriteLine("SKILL:        {0}", skill);
            Console.WriteLine("RANK:         {0}", rank);
        }


        #region FUNCTIONS

        // checks if logged in and if serverID is valid
        private static int ChecksID(string sID = null)
        {
            // check if we are logged in
            if (!bLoggedin)
            {
                PrintError("User not logged in!");
                return -1;
            }

            // try to parse sID to integer
            int iID = iLastServer;
            if (!string.IsNullOrEmpty(sID))
            {
                if (!Int32.TryParse(sID, out iID))
                {
                    PrintError("ServerID not valid!");
                    return -1;
                }
            }

            // iLastServer was never set
            if (iID == -1)
            {
                PrintError("No server selected!");
                return -1;
            }

            // checks if server is still in list
            // BF3
            if (iLoaded == 1)
            {
                if (!zloAPI.BF3.ServerList.ContainsKey(iID))
                {
                    PrintError("Selected server not in list!");
                    return -1;
                }
            }
            // BF4
            else if (iLoaded == 2)
            {
                if (!zloAPI.BF4.ServerList.ContainsKey(iID))
                {
                    PrintError("Selected server not in list!");
                    return -1;
                }
            }

            return iID;
        }


        // returns server-state from internal number
        private static string RealState(int state)
        {
            switch (state)
            {
                case 0:
                    return "WAITING";
                case 1:
                    return "INITGAME";
                case 130:
                    return "PRE_GAME";
                case 131:
                    return "IN_GAME";
                case 141:
                    return "POSTGAME";
                default:
                    return state.ToString();
            }
        }


        // callback for _reconnecttimer
        private static void ReconnectTimer(object source, ElapsedEventArgs e)
        {
            _reconnecttimer.Enabled = false;

            PrintLogInfo("Reconnecting...");

            // try to reconnect to master
            // BF3
            if (iLoaded == 1)
            {
                if (!zloAPI.BF3.ZLO_ConnectMServer())
                {
                    PrintLogError("Can't connect to master!");
                    PrintInfo("Will retry in 15 seconds...");

                    // set timer to 15sec interval
                    _reconnecttimer.Interval = 15000;
                    _reconnecttimer.Enabled = true;
                }
                else
                {
                    // set timer to 5sec interval
                    _reconnecttimer.Interval = 5000;
                    PrintLogInfo("Reconnect successful");

                    // clear screen
                    PrintServerMode();
                }
            }
            // BF4
            else if (iLoaded == 2)
            {
                if (!zloAPI.BF4.ZLO_ConnectMServer())
                {
                    PrintLogError("Can't connect to master!");
                    PrintInfo("Will retry in 15 seconds...");

                    // set timer to 15sec interval
                    _reconnecttimer.Interval = 15000;
                    _reconnecttimer.Enabled = true;
                }
                else
                {
                    // set timer to 5sec interval
                    _reconnecttimer.Interval = 5000;
                    PrintLogInfo("Reconnect successful");

                    // clear screen
                    PrintServerMode();
                }
            }
        }


        // callback for _redrawtimer
        private static void RedrawTimer(object source, ElapsedEventArgs e)
        {
            _redrawtimer.Enabled = false;

            // get current line
            int currentLinePos = Console.CursorTop;

            int i = 0;
            // refresh lines which contains serverinfos
            // BF3
            if (iLoaded == 1)
            {
                foreach (var server in zloAPI.BF3.ServerList)
                {
                    // set cursor to beginning of line 13 + i
                    Console.SetCursorPosition(0, (13 + i));
                    // clear line
                    Console.Write(new string(' ', Console.WindowWidth));
                    // set cursor back to start of line
                    Console.SetCursorPosition(0, (13 + i));

                    // write updated server-entry
                    // cut servername to a maximum of 47 chars
                    string sName = server.Value.name;
                    if (sName.Length > 47)
                        sName = sName.Substring(0, 47);
                    string sPlayers = "";

                    // add leading '0' if < 10 players or slots
                    if (server.Value.playercount < 10)
                        sPlayers = "0";
                    sPlayers += server.Value.playercount + "/";
                    if (server.Value.gamesize < 10)
                        sPlayers += "0";
                    sPlayers += server.Value.gamesize;

                    // IDs should be smaller than 6 chars, servername has been cutted to 47 chars, state is max. 8 chars
                    // longest mapname has 19 chars (Operation Firestorm), longest gametype has 22 chars (Conquest Assault Large) and gamesize is always 5 chars
                    if (RealMapNames.ContainsKey(server.Value.attr["level"]) && RealModeNames.ContainsKey(server.Value.attr["levellocation"]))
                        Console.WriteLine(" {0,-5}  {1,-47}  {2,-8}  {3,-19}  {4,-22}  {5,-5}", server.Key, sName, RealState(server.Value.state), RealMapNames[server.Value.attr["level"]], RealModeNames[server.Value.attr["levellocation"]], sPlayers);
                    else
                        Console.WriteLine(" {0,-5}  {1,-47}  {2,-8}  {3,-19}  {4,-22}  {5,-5}", server.Key, sName, RealState(server.Value.state), server.Value.attr["level"], server.Value.attr["levellocation"], sPlayers);
                    i++;
                }
            }
            // BF4
            else if (iLoaded == 2)
            {
                foreach (var server in zloAPI.BF4.ServerList)
                {
                    // set cursor to beginning of line 13 + i
                    Console.SetCursorPosition(0, (13 + i));
                    // clear line
                    Console.Write(new string(' ', Console.WindowWidth));
                    // set cursor back to start of line
                    Console.SetCursorPosition(0, (13 + i));

                    // write updated server-entry
                    // cut servername to a maximum of 46 chars
                    string sName = server.Value.name;
                    if (sName.Length > 46)
                        sName = sName.Substring(0, 46);
                    string sPlayers = "";

                    // add leading '0' if < 10 players or slots
                    if (server.Value.playercount < 10)
                        sPlayers = "0";
                    sPlayers += server.Value.playercount + "/";
                    if (server.Value.gamesize < 10)
                        sPlayers += "0";
                    sPlayers += server.Value.gamesize;

                    // IDs should be smaller than 6 chars, servername has been cutted to 46 chars, state is max. 8 chars
                    // longest mapname has 21 chars (Zavod:Graveyard Shift), longest gametype has 21 chars (Carrier Assault Large) and gamesize is always 5 chars
                    if (RealMapNames.ContainsKey(server.Value.attr["level"]) && RealModeNames.ContainsKey(server.Value.attr["levellocation"]))
                        Console.WriteLine(" {0,-5}  {1,-46}  {2,-8}  {3,-21}  {4,-21}  {5,-5}", server.Key, sName, RealState(server.Value.state), RealMapNames[server.Value.attr["level"]], RealModeNames[server.Value.attr["levellocation"]], sPlayers);
                    else
                        Console.WriteLine(" {0,-5}  {1,-46}  {2,-8}  {3,-21}  {4,-21}  {5,-5}", server.Key, sName, RealState(server.Value.state), server.Value.attr["level"], server.Value.attr["levellocation"], sPlayers);
                    i++;
                }
            }

            // set cursor back to original line
            if (currentLinePos > 14)
                Console.SetCursorPosition(0, currentLinePos);
            else
                Console.SetCursorPosition(0, currentLinePos + i + 2);
        }


        // callback for _closetimer
        private static void CloseTimer(object source, ElapsedEventArgs e, object args)
        {
            var parameters = args as object[];
            if (parameters == null)
                return;
            var mode = (bool) parameters[0];
            var name = (string) parameters[1];
            var title = (string) parameters[2];

            // get the process of game/server by executable-name
            var proc = Process.GetProcessesByName(name);
            if (proc.Length > 0)
            {
                // check if executable-title matches given title
                if (proc[0].MainWindowTitle.StartsWith(title))
                {
                    // check if process has valid handle
                    if (proc[0].MainWindowHandle != IntPtr.Zero)
                    {
                        // we can't check !proc[0].HasExited here as this requires full access to process which we won't have except user starts launcher as admin
                        // check if process is responding
                        if (IsResponding(proc[0]))
                        {
                            // exit function here as everything is alright
                            bFailedCheck = false;
                            return;
                        }
                    }
                }
            }

            // autoclose-mode for exit/crash game
            if (mode)
            {
                if (bFailedCheck)
                {
                    try
                    {
                        bExit = true;
                        BF3dll = null;
                        BF4dll = null;
                        Environment.Exit(0);
                    }
                    catch (Exception)
                    {
                        Environment.Exit(0);
                    }
                }
            }
            // autorestart-mode if server crashed
            else
            {
                // restart server
                if (bFailedCheck)
                {
                    
                }
            }

            // set a toggle-variable to true so we check a second time before killing/restarting to prevent false poitives
            bFailedCheck = true;
        }


        // checks if process is responding within 3 seconds (instead of 5sec of normal Process.Responding)
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessageTimeout(HandleRef hWnd, int msg, IntPtr wParam, IntPtr lParam, int flags, int timeout, out IntPtr pdwResult);
        const int SMTO_ABORTIFHUNG = 2;
        private static bool IsResponding(Process process)
        {
            HandleRef handleRef = new HandleRef(process, process.MainWindowHandle);

            int timeout = 3000;
            IntPtr lpdwResult;

            IntPtr lResult = SendMessageTimeout(
                handleRef,
                0,
                IntPtr.Zero,
                IntPtr.Zero,
                SMTO_ABORTIFHUNG,
                timeout,
                out lpdwResult);

            return lResult != IntPtr.Zero;
        }


        // prevents accidentally closing of console window by disabling the 'Close'-Button
        [DllImport("user32.dll")]
        public static extern int DeleteMenu(IntPtr hMenu, int nPosition, int wFlags);
        [DllImport("user32.dll")]
        private static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);
        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();
        private const int MF_BYCOMMAND = 0x00000000;
        private const int SC_CLOSE = 0xF060;
        private static void PrevenClosing()
        {
            DeleteMenu(GetSystemMenu(GetConsoleWindow(), false), SC_CLOSE, MF_BYCOMMAND);
        }


        #region PUBLIC FUNCTIONS

        // prints error messages
        public static void PrintError(string msg)
        {
            // check if user is in command prompt
            if (Console.CursorLeft == 4)
            {
                ClearLine();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("[!] ");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("ERROR: " + msg);
                PrintPromt();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("[!] ");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("ERROR: " + msg);
            }
        }


        // prints and logs error messages
        public static void PrintLogError(string msg)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("[!] ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("ERROR: " + msg);

            // log error message to file
            try
            {
                using (StreamWriter writer = new StreamWriter("zloLauncherCLI.log", true))
                {
                    writer.WriteLine("[" + DateTime.Now + "] " + msg);
                }
            }
            catch (Exception)
            {
                // nothing
            }
        }


        // prints warning messages
        public static void PrintWarning(string msg)
        {
            // check if user is in command prompt
            if (Console.CursorLeft == 4)
            {
                ClearLine();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write("[*] ");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("WARNING: " + msg);
                PrintPromt();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write("[*] ");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("WARNING: " + msg);
            }
        }


        // prints info messages
        public static void PrintInfo(string msg)
        {
            if (Console.CursorLeft >= 4)
                Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("[+] ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(msg);
        }


        // prints and logs info messages
        public static void PrintLogInfo(string msg)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("[+] ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(msg);

            // log error message to file
            try
            {
                using (StreamWriter writer = new StreamWriter("zloLauncherCLI.log", true))
                {
                    writer.WriteLine("[" + DateTime.Now + "] " + msg);
                }
            }
            catch (Exception)
            {
                // nothing
            }
        }


        // prints and logs info messages
        public static void LogInfo(string msg)
        {
            // log error message to file
            try
            {
                using (StreamWriter writer = new StreamWriter("zloLauncherCLI.log", true))
                {
                    writer.WriteLine("[" + DateTime.Now + "] " + msg);
                }
            }
            catch (Exception)
            {
                // nothing
            }
        }


        // prints input prompt
        public static void PrintInput(string msg)
        {
            if (Console.CursorLeft >= 4)
                Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("[~] ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write(msg + " ");
        }


        // prompt
        public static void PrintPromt()
        {
            if (Console.CursorLeft >= 4)
                Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("[>] ");
            Console.ForegroundColor = ConsoleColor.Gray;
        }


        // wait for input then exit
        public static void WaitInput()
        {
            Console.ReadLine();
            Environment.Exit(0);
        }


        // clears current console line
        public static void ClearLine()
        {
            int currentLinePos = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLinePos);
        }


        // print banner in servermode
        public static void PrintServerMode()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(@"           __      __                           __              ________    ____");
            Console.WriteLine(@"    ____  / /___  / /   ____ ___  ______  _____/ /_  ___  _____/ ____/ /   /  _/");
            Console.WriteLine(@"   /_  / / / __ \/ /   / __ `/ / / / __ \/ ___/ __ \/ _ \/ ___/ /   / /    / /  ");
            Console.WriteLine(@"    / /_/ / /_/ / /___/ /_/ / /_/ / / / / /__/ / / /  __/ /  / /___/ /____/ /   ");
            Console.WriteLine(@"   /___/_/\____/_____/\__,_/\__,_/_/ /_/\___/_/ /_/\___/_/   \____/_____/___/   ");
            Console.WriteLine(@"                                                                                ");
            Console.WriteLine("                        [ a CLI Launcher for zloemu.org ]");
            Console.WriteLine("                                   - by Despo -");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Gray;
            PrintInfo("ServerLauncher mode! Exit by pressing [Ctrl] + [E].");
            Console.WriteLine();
            // BF3
            if (iLoaded == 1)
            {
                Console.WriteLine(" ID     SERVERNAME                                       STATE     MAP                  GAMETYPE                PLAYERS");
                Console.WriteLine(" -----  -----------------------------------------------  --------  -------------------  ----------------------  -------");
            }
            // BF4
            else if (iLoaded == 2)
            {
                Console.WriteLine(" ID     SERVERNAME                                      STATE     MAP                    GAMETYPE               PLAYERS");
                Console.WriteLine(" -----  ----------------------------------------------  --------  ---------------------  ---------------------  -------");
            }
        }


        // update Launcher-DLL
        public static bool UpdateDLL(int iGame)
        {
            PrintLogInfo("Updating DLL...");
            string sDLL = "";
            string sLink = "";

            // BF3
            if (iGame == 1)
            {
                sDLL = "LauncherBF3.dll";
                sLink = "http://zloemu.org/files/bf3/Launcher.dll?d=";
            }
            // BF4
            else if (iGame == 2)
            {
                sDLL = "LauncherBF4.dll";
                sLink = "http://zloemu.org/files/bf4/Launcher.dll?d=";
            }

            // download DLL
            using (WebClient client = new WebClient())
            {
                Random r = new Random();

                try
                {
                    // if already downloaded delete old "new" file
                    if (File.Exists("new_" + sDLL))
                        File.Delete("new_" + sDLL);

                    // we add 7 random numbers so serverowner we can distingush between auto-downloaded DLLs and manually downloaded ones
                    client.DownloadFile(sLink + r.Next(9999999), "new_" + sDLL);
                }
                catch (Exception ex)
                {
                    PrintLogError("Error while trying to download DLL!");
                    PrintError(ex.Message);
                    return false;
                }
            }

            // if file was missing
            if (!File.Exists(sDLL))
            {
                // rename dll
                File.Move("new_" + sDLL, sDLL);
                return true;
            }
            if (File.Exists("new_" + sDLL))
            {
                // start a hidden cmd-operation which will delete old dll, rename new one and then start Launcher with the same arguments after 5 sec
                var Info = new ProcessStartInfo();
                string filename = Process.GetCurrentProcess().ProcessName + ".exe";
                Info.Arguments = "/C ping 127.0.0.1 -n 5 && del " + sDLL + " /F /Q && ren new_" + sDLL + " " + sDLL + " && " + "start " + filename + " " + sParameters;
                Info.WindowStyle = ProcessWindowStyle.Hidden;
                Info.CreateNoWindow = true;
                Info.FileName = "cmd.exe";
                Info.UseShellExecute = false;
                Process.Start(Info);
            }

            PrintLogInfo("DLL updated! Restarting...");

            // kill program
            Environment.Exit(0);
            return true;
        }

        #endregion

        #endregion
    }
}
