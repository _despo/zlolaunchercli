﻿/* 
    DATA CLASS FOR BATTLEFIELD 4
*/


namespace zloLauncherCLI.Data
{
    class BF4data
    {
        public static string[] MapNames = new string[]                                                  // all mapnames in server-format and alphabetical order to match RealMapNames
        {
            "XP1_002",                                              // Altai Range
            "XP0_Caspian",                                          // Caspian Border 2014
            "MP_Tremors",                                           // Dawnbreaker
            "XP1_004",                                              // Dragon Pass
            "XP7_Valley",                                           // Dragon Valley 2015
            "XP0_Firestorm",                                        // Firestorm 2014
            "MP_Flooded",                                           // Flood Zone
            "XP4_WalkerFactory",                                    // Giants of Karelia
            "MP_Journey",                                           // Golmud Railway
            "XP1_003",                                              // Guilin Peaks
            "XP0_Oman",                                             // Gulf of Oman 2014
            "MP_Resort",                                            // Hainan Resort
            "XP4_SubBase",                                          // Hammerhead
            "XP4_Titan",                                            // Hangar 21
            "MP_Damage",                                            // Lancang Dam
            "XP2_001",                                              // Lost Islands
            "XP3_UrbanGdn",                                         // Lumpini Garden
            "XP2_002",                                              // Nansha Strike
            "MP_Prison",                                            // Operation Locker
            "XP0_Metro",                                            // Operation Metro 2014
            "XP2_004",                                              // Operation Mortar
            "XP6_CMP",                                              // Operation Outbreak
            "XP4_Arctic",                                           // Operation Whiteout
            "MP_Naval",                                             // Paracel Storm
            "XP3_MarketPl",                                         // Pearl Market
            "XP3_Prpganda",                                         // Propaganda
            "MP_TheDish",                                           // Rogue Transmission
            "MP_Siege",                                             // Siege of Shanghai
            "XP1_001",                                              // Silk Road
            "XP3_WtrFront",                                         // Sunken Dragon
            "XP2_003",                                              // Wave Breaker
            "MP_Abandoned",                                         // Zavod 311
            "XP5_Night_01"                                          // Zavod: Graveyard Shift
        };


        public static string[] RealMapNames = new string[]                                              // all english mapnames in alphabetical order
        {
            "Altai Range",
            "Caspian Border 2014",
            "Dawnbreaker",
            "Dragon Pass",
            "Dragon Valley 2015",
            "Firestorm 2014",
            "Flood Zone",
            "Giants of Karelia",
            "Golmud Railway",
            "Guilin Peaks",
            "Gulf of Oman 2014",
            "Hainan Resort",
            "Hammerhead",
            "Hangar 21",
            "Lancang Dam",
            "Lost Islands",
            "Lumpini Garden",
            "Nansha Strike",
            "Operation Locker",
            "Operation Metro 2014",
            "Operation Mortar",
            "Operation Outbreak",
            "Operation Whiteout",
            "Paracel Storm",
            "Pearl Market",
            "Propaganda",
            "Rogue Transmission",
            "Siege of Shanghai",
            "Silk Road",
            "Sunken Dragon",
            "Wave Breaker",
            "Zavod 311",
            "Zavod:Graveyard Shift"
        };


        public static string[] ModeNames = new string[]                                                 // all gametypes in server-format and alphabetical order to match RealModeNames
        {
            "AirSuperiority0",                                      // Air Superiority
            "CaptureTheFlag0",                                      // Capture the flag
            "CarrierAssaultSmall0",                                 // Carrier Assault
            "CarrierAssaultLarge0",                                 // Carrier Assault Large
            "Chainlink0",                                           // Chain Link
            "ConquestSmall0",                                       // Conquest
            "ConquestLarge0",                                       // Conquest Large
            "Elimination0",                                         // Defuse
            "Domination0",                                          // Domination
            "GunMaster0",                                           // Gun Master
            "GunMaster1",                                           // Gun Master v2 - WARNING! - additional Gunmaster which is not distinguishable from normal GunMaster0
            "Obliteration",                                         // Obliteration
            "RushLarge0",                                           // Rush
            "SquadDeathMatch0",                                     // Squad Deathmatch
            "SquadDeathMatch1",                                     // Squad Deathmatch v2 - WARNING! - additional Squad Deathmatch which is not distinguishable from normal SquadDeathMatch0
            "SquadObliteration0",                                   // Squad Obliteration
            "TeamDeathMatch0",                                      // Team Deathmatch
            "TeamDeathMatch1"                                       // Team Deathmatch v2 - WARNING! - additional Team Deathmatch which is not distinguishable from normal TeamDeathMatch0
        };


        public static string[] RealModeNames = new string[]                                             // all english gamemodes in alphabetical order
        {
            "Air Superiority",
            "Capture the flag",
            "Carrier Assault",
            "Carrier Assault Large",
            "Chain Link",
            "Conquest",
            "Conquest Large",
            "Defuse",
            "Domination",
            "Gun Master",
            "Gun Master v2",
            "Obliteration",
            "Rush",
            "Squad Deathmatch",
            "Squad Deathmatch v2",
            "Squad Obliteration",
            "Team Deathmatch",
            "Team Deathmatch v2"
       };
    }
}
