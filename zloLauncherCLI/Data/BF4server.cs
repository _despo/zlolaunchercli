﻿/* 
    SERVERDATA CLASS FOR BATTLEFIELD 4
*/

using System.Collections.Generic;


namespace zloLauncherCLI.Data
{
    class BF4server
    {
        public int gamesize;                                                        // ServerListenerCap - cap0
        public string ip;                                                           // ServerListenerAddr - ip
        public string name;                                                         // ServerListenerName - name
        public int playercount;                                                     // ServerListenerPlayers - players
        public int port;                                                            // ServerListenerAddr - port
        public int state;                                                           // ServerListenerState - state
        public Dictionary<string, string> attr = new Dictionary<string, string>();  // ServerListenerAttr - name, value


        // initialize serverentry
        public BF4server()
        {
            gamesize = playercount = port = state = 0;
            ip = "?";
            name = "0";
            attr.Add("preset", "");
            attr.Add("levellocation", "");
            attr.Add("level", "");
            attr.Add("punkbuster", "");
            attr.Add("fairfight", "");
            attr.Add("servertype", "");
            attr.Add("settings1", "");
            attr.Add("tickRate", "");
        }
    }
}
